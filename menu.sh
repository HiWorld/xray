#!/bin/bash
clear
c='\e[96m'     # Cyan
gb='\e[1;92m'   # Green
p='\e[95m'     # Purple
r='\e[91m'     # Red
rb='\e[41m'
wb='\e[1;97m'
yb='\e[1;93m'
nc='\e[0m'  # Reset to default color

# Assign system information to variables
OS=$(awk -F= '/PRETTY_NAME/{print $2}' /etc/os-release | tr -d \")
RAM=$(free -h | awk '/Mem/{printf "%-5s / %-5s (%.1f%%)", $2, $3, $3/$2 * 100}')
CPU=$(grep -c processor /proc/cpuinfo)" Core(s)"
CPU_USAGE=$(top -bn1 | grep "Cpu(s)" | awk '{print 100-$8"%"}')
UPTIME=$(uptime -p)
TIME=$(date +"%A, %d %B %Y %T %Z")
VKERNEL=$(uname -r)
TCP_CC=$(sysctl net.ipv4.tcp_congestion_control | awk '{print $3}')
# Assign network information to variables
ISP=$(curl -s https://ipapi.co/org/)
CITY=$(curl -s https://ipapi.co/city/)
IP=$(curl -s https://ipapi.co/ip/)
DOMAIN=$(cat /etc/xray/domain.txt)
nginx_status() {
  systemctl is-active --quiet nginx && echo "${gb}Active${nc}" || echo "${r}Inactive${nc}"
}
xray_status() {
  systemctl is-active --quiet xray && echo "${gb}Active${nc}" || echo "${r}Inactive${nc}"
}

vmess=$(ls -1 /etc/xray/vmess/*.json 2>/dev/null | wc -l)
vless=$(ls -1 /etc/xray/vless/*.json 2>/dev/null | wc -l)
trojan=$(ls -1 /etc/xray/trojan/*.json 2>/dev/null | wc -l)
ss=$(ls -1 /etc/xray/ss/*.json 2>/dev/null | wc -l)
# Display information in an ASCII box
echo -e "${c}╔════════════════════════════════════╗${nc}"
echo -e "${c}║${rb}${wb}        System Configuration        ${nc}${c}║${nc}"
echo -e "${c}╟────────────────────────────────────╢${nc}"
echo -e "${c}║ OS: ${nc}$OS"
echo -e "${c}║ KERNEL VERSION: ${nc}$VKERNEL"
echo -e "${c}║ TCP CC: ${nc}$TCP_CC"
echo -e "${c}║ RAM: ${nc}$RAM"
echo -e "${c}║ CPU: ${nc}$CPU"
echo -e "${c}║ CPU USAGE: ${nc}$CPU_USAGE"
echo -e "${c}║ UPTIME: ${nc}$UPTIME"
echo -e "${c}║ CURRENT TIME: ${nc}$TIME"
echo -e "${c}╙────────────────────────────────────╜${nc}"
echo -e "${c}╓────────────────────────────────────╖${nc}"
echo -e "${c}║${rb}${wb}       Network Configuration        ${nc}${c}║${nc}"
echo -e "${c}╟────────────────────────────────────╢${nc}"
echo -e "${c}║ ISP: ${nc}$ISP"
echo -e "${c}║ City: ${nc}$CITY"
echo -e "${c}║ Public IP: ${nc}$IP"
echo -e "${c}║ Domain: ${nc}$DOMAIN"
echo -e "${c}╙────────────────────────────────────╜${nc}"
echo -e "${c}╓────────────────────────────────────╖${nc}"
echo -e "${c}║${p} Nginx${nc} : $(nginx_status)     ${p}Xray${nc} : $(xray_status)   ${nc}"
echo -e "${c}╙────────────────────────────────────╜${nc}"
# Display new menu options
echo -e "${c}╓────────────────────────────────────╖${nc}"
echo -e "${c}║ ${wb}vmess : ${gb}${vmess} ${nc}     ${wb}vless : ${gb}${vless} ${nc}"
echo -e "${c}║ ${wb}trojan : ${gb}${trojan} ${nc}    ${wb}shadowsocks: ${gb}${ss} ${nc}"
echo -e "${c}╟────────────────────────────────────╢${nc}"
echo -e "${c}║${rb}${wb}             Main Menu              ${nc}${c}║${nc}"
echo -e "${c}╟────────────────────────────────────╢${nc}"
echo -e "${c}║ ${wb}[1]${yb} VMESS       ${nc}║ ${wb}[6]${yb} Enable bbr   ${nc}"
echo -e "${c}║ ${wb}[2]${yb} VLESS       ${nc}║ ${wb}[7]${yb} Disable bbr  ${nc}"
echo -e "${c}║ ${wb}[3]${yb} Trojan      ${nc}║ ${wb}[8]${yb} Restart service  ${nc}"
echo -e "${c}║ ${wb}[4]${yb} Shadowsocks ${nc}║ ${wb}[9]${yb} Bandwidth usage ${nc}"
echo -e "${c}║ ${wb}[5]${yb} Speedtest   ${nc}║ ${wb}[10]${yb} Exit    ${nc}"
echo -e "${c}╙────────────────────────────────────╜${nc}"
read -p "Enter your choice: " choice
while true; do
  case $choice in
    1)
      menu-vmess
      break;;
    2)
      menu-vless
      break;;
    3)
      menu-trojan
      break;;
    4)
      menu-ss
      break;;
    5)
      clear;speedtest --share
      break;;
    6)
      enable-bbr
      break;;
    7)
      disable-bbr
      break;;
    8)
      service-restart
      break;;
    9)
      bw-usage
      break;;
    10)
      echo "Exiting..."
      exit;;
    *)
      echo "Invalid choice. Exiting..."
      sleep2;menu;break;;
  esac
done