#!/bin/bash
echo "[+] Checking permission"
if [ $EUID -ne 0 ];
  then
  echo "[!] please run this script as a root user!"
  exit
fi
sleep 2
echo "[+] Permission accepted"
sleep 1
echo "[+] Disable ipv6"
sudo sysctl -w net.ipv6.conf.all.disable_ipv6=1
sudo sysctl -w net.ipv6.conf.default.disable_ipv6=1
sleep 0.5
echo "[+] Updating repository"
apt update;apt upgrade -y
clear
echo "[+] Installing required packages"
sleep 2
sudo apt -y install nginx && sudo apt -y install cron && sudo apt -y install zip && sudo apt -y install unzip && sudo apt -y install dpkg && sudo apt -y install curl && sudo apt -y install wget && sudo apt -y install systemd-timesyncd && sudo apt -y install python3 && sudo apt -y install socat && sudo apt -y install net-tools && sudo apt -y install speedtest-cli && sudo apt -y install vnstat && sudo apt -y install ntp
clear
echo "[+] Setting up time zone"
sudo timedatectl set-timezone Asia/Jakarta
sleep 0.5
echo "[+] Creating swapfile"
sudo fallocate -l 1G /swapfile
sudo chmod 600 /swapfile
sudo mkswap /swapfile
sudo swapon /swapfile
echo '/swapfile none swap sw 0 0' | sudo tee -a /etc/fstab
sleep 0.5
echo "[+] Installing Xray & setting up xray configuration....."
wget https://github.com/XTLS/Xray-core/releases/latest/download/Xray-linux-64.zip
unzip Xray-linux-64.zip -d /usr/bin/
rm -f Xray-linux-64.zip
mkdir -p /etc/xray
mkdir -p /var/log/xray
mkdir -p /etc/xray/vless
mkdir -p /etc/xray/vmess
mkdir -p /etc/xray/ss
mkdir -p /etc/xray/trojan
wget -O /etc/xray/config.json https://gitlab.com/HiWorld/xray/-/raw/main/config.json
cat > /etc/systemd/system/xray.service << EOF
[Unit]
Description=xray Service
After=network.target

[Service]
User=root
Group=root
Restart=always
ExecStart=/usr/bin/xray run -c /etc/xray/config.json
RestartSec=10s
[Install]
WantedBy=multi-user.target
EOF
sleep 0.5
clear
while true; do
    echo "[+] Generate domain cert"
    read -p "Input Domain : " domain
    echo "[!] Stoping nginx...."
    systemctl stop nginx
    curl https://get.acme.sh | sh
    source ~/.bashrc
    ~/.acme.sh/acme.sh --register-account -m ardanferdi19@gmail.com
    ~/.acme.sh/acme.sh --issue -d $domain --standalone --server letsencrpyt \
      --key-file /etc/xray/xray.key \
      --fullchain-file /etc/xray/xray.crt
    if [ $? -ne 0 ]; then
        sleep 3
        clear
        echo "[!] Certificate generation failed. Retrying..."
    else
        echo "[+] Certificate generated successfully"
        echo "$domain" > /etc/xray/domain.txt
        break
    fi
done
sleep 3
clear
echo "[+] Setting up nginx server"
wget -O /etc/nginx/nginx.conf https://gitlab.com/HiWorld/xray/-/raw/main/nginx.conf
wget -O /etc/nginx/conf.d/xray.conf https://gitlab.com/HiWorld/xray/-/raw/main/xray.conf
wget -O /var/www/html/index.html https://gitlab.com/HiWorld/xray/-/raw/main/index.html
sleep 1
clear
echo "[+] Setting up auto exp"
wget -O /etc/xray/auto-exp https://gitlab.com/HiWorld/xray/-/raw/main/auto-exp.sh
chmod +x /etc/xray/auto-exp
echo "0 0 * * * root /etc/xray/auto-exp" | sudo tee -a /etc/crontab > /dev/null
systemctl restart cron
echo "[+] Downloading menu"
wget -O /usr/bin/add-vmess https://gitlab.com/HiWorld/xray/-/raw/main/add-vmess.sh
wget -O /usr/bin/add-vless https://gitlab.com/HiWorld/xray/-/raw/main/add-vless.sh
wget -O /usr/bin/add-trojan https://gitlab.com/HiWorld/xray/-/raw/main/add-trojan.sh
wget -O /usr/bin/add-ss https://gitlab.com/HiWorld/xray/-/raw/main/add-ss.sh
wget -O /usr/bin/menu-vmess https://gitlab.com/HiWorld/xray/-/raw/main/menu-vmess.sh
wget -O /usr/bin/menu-vless https://gitlab.com/HiWorld/xray/-/raw/main/menu-vless.sh
wget -O /usr/bin/menu-trojan https://gitlab.com/HiWorld/xray/-/raw/main/menu-trojan.sh
wget -O /usr/bin/menu-ss https://gitlab.com/HiWorld/xray/-/raw/main/menu-ss.sh
wget -O /usr/bin/del-vmess https://gitlab.com/HiWorld/xray/-/raw/main/del-vmess.sh
wget -O /usr/bin/del-vless https://gitlab.com/HiWorld/xray/-/raw/main/del-vless.sh
wget -O /usr/bin/del-trojan https://gitlab.com/HiWorld/xray/-/raw/main/del-trojan.sh
wget -O /usr/bin/del-ss https://gitlab.com/HiWorld/xray/-/raw/main/del-ss.sh
wget -O /usr/bin/chk-vmess https://gitlab.com/HiWorld/xray/-/raw/main/chk-vmess.sh
wget -O /usr/bin/chk-vless https://gitlab.com/HiWorld/xray/-/raw/main/chk-vless.sh
wget -O /usr/bin/chk-trojan https://gitlab.com/HiWorld/xray/-/raw/main/chk-trojan.sh
wget -O /usr/bin/chk-ss https://gitlab.com/HiWorld/xray/-/raw/main/chk-ss.sh
wget -O /usr/bin/enable-bbr https://gitlab.com/HiWorld/xray/-/raw/main/enable-bbr.sh
wget -O /usr/bin/disable-bbr https://gitlab.com/HiWorld/xray/-/raw/main/disable-bbr.sh
wget -O /usr/bin/service-restart https://gitlab.com/HiWorld/xray/-/raw/main/service-restart.sh
wget -O /usr/bin/bw-usage https://gitlab.com/HiWorld/xray/-/raw/main/bw-usage.sh
wget -O /usr/bin/menu https://gitlab.com/HiWorld/xray/-/raw/main/menu.sh
chmod +x /usr/bin/add-vmess
chmod +x /usr/bin/add-vless
chmod +x /usr/bin/add-trojan
chmod +x /usr/bin/add-ss
chmod +x /usr/bin/menu-vmess
chmod +x /usr/bin/menu-vless
chmod +x /usr/bin/menu-trojan
chmod +x /usr/bin/menu-ss
chmod +x /usr/bin/del-vmess
chmod +x /usr/bin/del-vless
chmod +x /usr/bin/del-trojan
chmod +x /usr/bin/del-ss
chmod +x /usr/bin/chk-vmess
chmod +x /usr/bin/chk-vless
chmod +x /usr/bin/chk-trojan
chmod +x /usr/bin/chk-ss
chmod +x /usr/bin/enable-bbr
chmod +x /usr/bin/disable-bbr
chmod +x /usr/bin/service-restart
chmod +x /usr/bin/bw-usage
chmod +x /usr/bin/menu
cat >> ~/.bashrc <<EOF
clear && menu
EOF
sleep 0.5
clear
echo "[+] Enable service"
systemctl daemon-reload
systemctl enable xray
systemctl enable nginx
sleep 1
clear
echo "[+] Restarting service"
systemctl start xray
systemctl start nginx
